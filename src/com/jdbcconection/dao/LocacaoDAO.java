package com.jdbcconection.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jdbcconection.conexao.Conexao;
import com.jdbcconection.model.Locacao;


public class LocacaoDAO {
	  private Connection connection;
	   
	    
	    public LocacaoDAO(){
	        this.connection = new Conexao().getConnection();
	    }
	    
	          public void adiciona(Locacao locacao){

	                String sql = "insert into locacao(cupom, cpf, codLivro, dataLocacao, dataPreviaDevolucao, dataRealDevolucao, multa) values(?, ?, ?, ?, ?, ?, ?)";
	                Locacao loc = getLocacao(locacao.getCupom());
	                if(loc == null){
	                	try {
	                		
	                		PreparedStatement stmt = connection.prepareStatement(sql);
	                		
	                		stmt.setString(1, locacao.getCupom());
	                		stmt.setString(2, locacao.getCpf());
	                		stmt.setInt(3, locacao.getCodLivro());
	                		stmt.setDate(4, (Date) locacao.getDataLocacao());
	                		stmt.setDate(5,  (Date) locacao.getDataPreviaDevolucao());
	                		stmt.setDate(6, (Date) locacao.getDataRealDevolucao());
	                		stmt.setFloat(7,  locacao.getMulta());
	                		
	                		
	                		stmt.execute();
	                		stmt.close();
	                		
	                	} catch (Exception e) {
	                		throw new RuntimeException(e);
	                	}
	                }else{
	                	System.out.println("Cupom Loca��o J� utilizado!");
	                }

	        }


	    public Locacao getLocacao(String cupom) {
	        
	        String sql = "select * from locacao where cupom = ?";
	        PreparedStatement stmt;
	        try {
	            stmt = connection.prepareStatement(sql);
	            stmt.setString(1, cupom);
	            ResultSet rs;
	            rs = stmt.executeQuery();
	            if (rs.next()) {
	            	
	                Locacao loc = new Locacao();
	                loc.setCupom(rs.getString("cupom"));
	                loc.setCpf(rs.getString("cpf"));
	                loc.setCodLivro(rs.getInt("codLivro"));
	                loc.setDataLocacao(rs.getDate("dataLocacao"));
	                loc.setDataPreviaDevolucao(rs.getDate("dataPreviaDevolucao"));
	                loc.setDataRealDevolucao(rs.getDate("dataRealDevolucao"));
	                loc.setMulta(rs.getFloat("multa"));
	                
	                return loc;
	            }
	        } catch (SQLException ex) {
	           ex.getMessage();
	        }
	        return null;
	    }
	    
	      
	    public List<Locacao> getListaLocacao() throws SQLException {
	        PreparedStatement stmt = this.connection.prepareStatement("SELECT * FROM locacao");
	        ResultSet rs = stmt.executeQuery();

	        List<Locacao> locacoes = new ArrayList<Locacao>();
	        while (rs.next()) {

	            //Criando um objeto tipo locacao  
	        	Locacao loc = montarObjetoLocacao(rs);
	            //Adicionando Valores a lista  
	        	locacoes.add(loc);
	        }
	        rs.close();
	        stmt.close();
	        return locacoes;
	    }
	    
	    private Locacao montarObjetoLocacao(ResultSet rs) throws SQLException{
	    	Locacao loc = new Locacao();
            loc.setCupom(rs.getString("cupom"));
            loc.setCpf(rs.getString("cpf"));
            loc.setCodLivro(rs.getInt("codLivro"));
            loc.setDataLocacao(rs.getDate("dataLocacao"));
            loc.setDataPreviaDevolucao(rs.getDate("dataPreviaDevolucao"));
            loc.setDataRealDevolucao(rs.getDate("dataRealDevolucao"));
            loc.setMulta(rs.getFloat("multa"));
            return loc;
	    }
	    
	    public int alteraLocacao(Locacao locacao) {

	        String sql = "UPDATE locacao SET cupom=?, cpf=?, codLivro=?, dataLocacao=?, dataPreviaDevolucao=?, dataRealDevolucao=?, multa=? WHERE cupom=? ;";
	        
	        int result = 0;
	        try {
	        	 PreparedStatement stmt = connection.prepareStatement(sql);

	        	stmt.setString(1, locacao.getCupom());
         		stmt.setString(2, locacao.getCpf());
         		stmt.setInt(3, locacao.getCodLivro());
         		stmt.setDate(4, (Date) locacao.getDataLocacao());
         		stmt.setDate(5,  (Date) locacao.getDataPreviaDevolucao());
         		stmt.setDate(6, (Date) locacao.getDataRealDevolucao());
         		stmt.setFloat(7,  locacao.getMulta());


                 stmt.execute();
                 stmt.close();
	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }
	    
	    public int excluir(String cupom) {

	        String sql = "DELETE FROM locacao WHERE cupom = ?";
	        int result = 0;
	        try {
	            PreparedStatement stmt = connection.prepareStatement(sql);

	            stmt.setString(1, cupom);

	            result = stmt.executeUpdate();

	            stmt.execute();
	            stmt.close();

	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }

	    
}

