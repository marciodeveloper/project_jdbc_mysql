package com.jdbcconection.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jdbcconection.conexao.Conexao;
import com.jdbcconection.model.Usuario;


public class UsuarioDAO {
	  private Connection connection;
	   
	    
	    public UsuarioDAO(){
	        this.connection = new Conexao().getConnection();
	    }
	    
	          public void adiciona(Usuario usuario){

	                String sql = "insert into usuario(cpf, nome, rua, bairro, cidade, matricula, periodo, inicioCurso, terminoCurso) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	                Usuario user = getUsuario(usuario.getCpf());
	                if(user == null){
	                	try {
	                		
	                		PreparedStatement stmt = connection.prepareStatement(sql);
	                		
	                		stmt.setString(1, usuario.getCpf());
	                		stmt.setString(2, usuario.getNome());
	                		stmt.setString(3, usuario.getRua());
	                		stmt.setString(4, usuario.getBairro());
	                		stmt.setString(5, usuario.getCidade());
	                		stmt.setString(6, usuario.getMatricula());
	                		stmt.setInt(7, usuario.getPeriodo());
	                		stmt.setDate(8,  (Date) usuario.getInicioCurso());
	                		stmt.setDate(9,  (Date) usuario.getTerminoCurso());
	                		
	                		stmt.execute();
	                		stmt.close();
	                		
	                	} catch (Exception e) {
	                		throw new RuntimeException(e);
	                	}
	                }else{
	                	System.out.println("Cpf j� cadastrado!");
	                }

	        }


	    public Usuario getUsuario(String cpf) {
	        
	        String sql = "select * from usuario where cpf = ?";
	        PreparedStatement stmt;
	        try {
	            stmt = connection.prepareStatement(sql);
	            stmt.setString(1, cpf);
	            ResultSet rs;
	            rs = stmt.executeQuery();
	            if (rs.next()) {
	            	
	                Usuario user = new Usuario();
	                user.setCpf(rs.getString("cpf"));
	                user.setNome(rs.getString("nome"));
	                user.setRua(rs.getString("rua"));
	                user.setBairro(rs.getString("bairro"));
	                user.setCidade(rs.getString("cidade"));
	                user.setMatricula(rs.getString("matricula"));
	                user.setPeriodo(rs.getInt("periodo"));
	                user.setInicioCurso(rs.getDate("inicioCurso"));
	                user.setTerminoCurso(rs.getDate("terminoCurso"));
	                return user;
	            }
	        } catch (SQLException ex) {
	           ex.getMessage();
	        }
	        return null;
	    }
	    
	    public List<Usuario> getListaUsuario() throws SQLException {
	        PreparedStatement stmt = this.connection.prepareStatement("SELECT * FROM usuario");
	        ResultSet rs = stmt.executeQuery();

	        List<Usuario> usuarios = new ArrayList<Usuario>();
	        while (rs.next()) {

	            //Criando um objeto tipo usuario  
	            Usuario usuario = montarObjetoUsuario(rs);
	            //Adicionando Valores a lista  
	            usuarios.add(usuario);
	        }
	        rs.close();
	        stmt.close();
	        return usuarios;
	    }
	    
	    private Usuario montarObjetoUsuario(ResultSet rs) throws SQLException{
	    	Usuario user = new Usuario();
            user.setCpf(rs.getString("cpf"));
            user.setNome(rs.getString("nome"));
            user.setRua(rs.getString("rua"));
            user.setBairro(rs.getString("bairro"));
            user.setCidade(rs.getString("cidade"));
            user.setMatricula(rs.getString("matricula"));
            user.setPeriodo(rs.getInt("periodo"));
            user.setInicioCurso(rs.getDate("inicioCurso"));
            user.setTerminoCurso(rs.getDate("terminoCurso"));
            return user;
	    }
	    
	    public int altera(Usuario usuario) {

	        String sql = "UPDATE usuario SET cpf=?, nome=?, rua=?, bairro=?, cidade=?, matricula=?, periodo=?, inicioCurso=?, terminoCurso=? WHERE cpf=? ;";
	        
	        int result = 0;
	        try {
	        	 PreparedStatement stmt = connection.prepareStatement(sql);

                 stmt.setString(1, usuario.getCpf());
                 stmt.setString(2, usuario.getNome());
                 stmt.setString(3, usuario.getRua());
                 stmt.setString(4, usuario.getBairro());
                 stmt.setString(5, usuario.getCidade());
                 stmt.setString(6, usuario.getMatricula());
                 stmt.setInt(7, usuario.getPeriodo());
                 stmt.setDate(8,  (Date) usuario.getInicioCurso());
                 stmt.setDate(9,  (Date) usuario.getTerminoCurso());

                 stmt.execute();
                 stmt.close();
	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }
	    
	    public int excluir(String cpfUsuario) {

	        String sql = "DELETE FROM usuario WHERE cpf = ?";
	        int result = 0;
	        try {
	            PreparedStatement stmt = connection.prepareStatement(sql);

	            stmt.setString(1, cpfUsuario);

	            result = stmt.executeUpdate();

	            stmt.execute();
	            stmt.close();

	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }

	    
}

