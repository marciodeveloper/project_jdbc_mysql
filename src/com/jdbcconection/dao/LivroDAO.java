package com.jdbcconection.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jdbcconection.conexao.Conexao;
import com.jdbcconection.model.Livro;


public class LivroDAO {
	  private Connection connection;
	   
	    
	    public LivroDAO(){
	        this.connection = new Conexao().getConnection();
	    }
	    
	          public void adiciona(Livro livro){

	                String sql = "insert into livro(codLivro, volume, isbn, disponivel, autor, nome) values(?, ?, ?, ?, ?, ?)";
	                Livro liv = getLivro(livro.getCodLivro());
	                if(liv == null){
	                	try {
	                		
	                		PreparedStatement stmt = connection.prepareStatement(sql);
	                		
	                		stmt.setInt(1, livro.getCodLivro());
	                		stmt.setInt(2, livro.getVolume());
	                		stmt.setString(3, livro.getIsbn());
	                		stmt.setString(4, livro.getDisponivel());
	                		stmt.setString(5, livro.getAutor());
	                		stmt.setString(6,  livro.getNome());
	                		
	                		
	                		stmt.execute();
	                		stmt.close();
	                		
	                	} catch (Exception e) {
	                		throw new RuntimeException(e);
	                	}
	                }else{
	                	System.out.println("Livro j� cadastrado!");
	                }

	        }


	    public Livro getLivro(int codLivro) {
	        
	        String sql = "select * from livro where codLivro = ?";
	        PreparedStatement stmt;
	        try {
	            stmt = connection.prepareStatement(sql);
	            stmt.setInt(1, codLivro);
	            ResultSet rs;
	            rs = stmt.executeQuery();
	            if (rs.next()) {
	            	
	                Livro livro = new Livro();
	                livro.setCodLivro(rs.getInt("codLivro"));
	                livro.setVolume(rs.getInt("volume"));
	                livro.setIsbn(rs.getString("isbn"));
	                livro.setDisponivel(rs.getString("disponivel"));
	                livro.setAutor(rs.getString("autor"));
	                livro.setNome(rs.getString("nome"));
	                return livro;
	            }
	        } catch (SQLException ex) {
	           ex.getMessage();
	        }
	        return null;
	    }
	    
	   
	    public List<Livro> getListaLivro() throws SQLException {
	        PreparedStatement stmt = this.connection.prepareStatement("SELECT * FROM livro");
	        ResultSet rs = stmt.executeQuery();

	        List<Livro> livros = new ArrayList<Livro>();
	        while (rs.next()) {

	            //Criando um objeto tipo livro  
	            Livro livro = montarObjetoLivro(rs);
	            //Adicionando Valores a lista  
	            livros.add(livro);
	        }
	        rs.close();
	        stmt.close();
	        return livros;
	    }
	    
	    private Livro montarObjetoLivro(ResultSet rs) throws SQLException{
	    	Livro livro = new Livro();
	    	livro.setCodLivro(rs.getInt("codLivro"));
	    	livro.setVolume(rs.getInt("volume"));
	    	livro.setDisponivel(rs.getString("disponivel"));
	    	livro.setIsbn(rs.getString("isbn"));
	    	livro.setAutor(rs.getString("autor"));
	    	livro.setNome(rs.getString("nome"));
            return livro;
	    }
	    
	    public int altera(Livro livro) {

	        String sql = "UPDATE livro SET codLivro=?, volume=?, isbn=?, disponivel=?, autor=? WHERE codLivro=? ;";
	        
	        int result = 0;
	        try {
	        	 PreparedStatement stmt = connection.prepareStatement(sql);

                 stmt.setInt(1, livro.getCodLivro());
                 stmt.setInt(2, livro.getVolume());
                 stmt.setString(3, livro.getIsbn());
                 stmt.setString(4, livro.getDisponivel());
                 stmt.setString(5,  livro.getAutor());
                 stmt.setString(6, livro.getNome());


                 stmt.execute();
                 stmt.close();
	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }
	    
	    public int excluir(int codLivro) {

	        String sql = "DELETE FROM livro WHERE codLivro = ?";
	        int result = 0;
	        try {
	            PreparedStatement stmt = connection.prepareStatement(sql);

	            stmt.setInt(1, codLivro);

	            result = stmt.executeUpdate();

	            stmt.execute();
	            stmt.close();

	        } catch (SQLException u) {
	            throw new RuntimeException(u);
	        }
	        return result;
	    }

	    
}

