package com.jdbcconection.model;

import java.sql.Date;

public class Locacao {
	
	private String cupom;
	private String cpf;
	private int codLivro;
	private Date dataLocacao;
	private Date dataPreviaDevolucao;
	private Date dataRealDevolucao;
	private float multa;
	
	public String getCupom() {
		return cupom;
	}
	public void setCupom(String cupom) {
		this.cupom = cupom;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getCodLivro() {
		return codLivro;
	}
	public void setCodLivro(int codLivro) {
		this.codLivro = codLivro;
	}
	public Date getDataLocacao() {
		return dataLocacao;
	}
	public void setDataLocacao(Date dataLocacao) {
		this.dataLocacao = dataLocacao;
	}
	public Date getDataPreviaDevolucao() {
		return dataPreviaDevolucao;
	}
	public void setDataPreviaDevolucao(Date dataPreviaDevolucao) {
		this.dataPreviaDevolucao = dataPreviaDevolucao;
	}
	public Date getDataRealDevolucao() {
		return dataRealDevolucao;
	}
	public void setDataRealDevolucao(Date dataRealDevolucao) {
		this.dataRealDevolucao = dataRealDevolucao;
	}
	public float getMulta() {
		return multa;
	}
	public void setMulta(float multa) {
		this.multa = multa;
	}
	
	
	

}
