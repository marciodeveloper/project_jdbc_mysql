package com.jdbcconection.model;

import java.sql.Date;

public class Usuario {

	private Long id;
	private String cpf;
	private String nome;
	private String rua;
	private String bairro;
	private String cidade;
	private String matricula;
	private int periodo;
	private Date inicioCurso;
	private Date terminoCurso;
	
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return this.id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public int getPeriodo() {
		return periodo;
	}
	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}
	public Date getInicioCurso() {
		return inicioCurso;
	}
	public void setInicioCurso(Date inicioCurso) {
		this.inicioCurso = inicioCurso;
	}
	public Date getTerminoCurso() {
		return terminoCurso;
	}
	public void setTerminoCurso(Date terminoCurso) {
		this.terminoCurso = terminoCurso;
	}
	
	
}

