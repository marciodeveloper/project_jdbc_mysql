package com.jdbcconection.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException; 

public class Conexao {
	
	 public Connection getConnection() {
	        try {
	            return DriverManager.getConnection("jdbc:mysql://localhost/projetoBiblioteca", "root", "root");
	        }
	        catch(SQLException excecao) {
	            throw new RuntimeException(excecao);
	        }
	    }

}
