package com.jdbcconection.teste;

import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.jdbcconection.dao.LivroDAO;
import com.jdbcconection.dao.LocacaoDAO;
import com.jdbcconection.dao.UsuarioDAO;
import com.jdbcconection.model.Livro;
import com.jdbcconection.model.Locacao;
import com.jdbcconection.model.Usuario;

public class Teste {

	private static UsuarioDAO usuarioDAO = new UsuarioDAO();
	private static LivroDAO livroDAO = new LivroDAO();
	private static LocacaoDAO locacaoDAO = new LocacaoDAO();
	
	private static void listaUsuarioPorCpf(String cpf){
		Usuario usuario = usuarioDAO.getUsuario(cpf);
		System.out.println("NOME: "+usuario.getNome()+" - CPF: "+usuario.getCpf());
	}
	
	private static Date converteStringEmData(String dataEmString){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
		Date dt = null;
		try {
			dt =  new java.sql.Date(df.parse(dataEmString).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dt;
	}
	
	private static void adicionarUsuario(){
		 Scanner sc = new Scanner(System.in);
		 Usuario user = new Usuario();
	        
	     System.out.print("Digite o CPF do usuario: \n");
         user.setCpf(sc.nextLine());
         
         System.out.print("Digite o NOME do usuario: \n");
         user.setNome(sc.nextLine());
         
         System.out.print("Digite a RUA do usuario: \n");
         user.setRua(sc.nextLine());
         
         System.out.print("Digite o BAIRRO do usuario: \n");
         user.setBairro(sc.nextLine());
         
         System.out.print("Digite a CIDADE do usuario: \n");
         user.setCidade(sc.nextLine());
         
         System.out.print("Digite a MATRICULA do usuario: \n");
         user.setMatricula(sc.nextLine());
         
         System.out.print("Digite o PERIODO: \n");
         user.setPeriodo(sc.nextInt());
         
         System.out.println("Digite a data de Inicio do Curso: EX dd/mm/aaaa \n");
         String inicioCurso = sc.next();
         
         System.out.println("Digite a data de termino do Curso: EX dd/mm/aaaa \n");
         String terminoCurso = sc.next();
         
         user.setInicioCurso(converteStringEmData(inicioCurso));
         user.setTerminoCurso(converteStringEmData(terminoCurso));
         usuarioDAO.adiciona(user);
         System.out.print("Usuario adicionado com Sucesso! \n");
	}
	
	private static void adicionaLivro(){
		Scanner sc = new Scanner(System.in);
		Livro livro = new Livro();
	        
	    System.out.print("Digite o codigo do Livro: \n");
	    livro.setCodLivro(sc.nextInt());
	    
	    System.out.print("Digite o ISBN do Livro: \n");
	    livro.setIsbn(sc.next());
	    
	    
	    System.out.print("Digite o volume do Livro: \n");
	    livro.setVolume(sc.nextInt());
	    
        
        System.out.print("Digite a disponibilidade do Livro: EX S(sim) ou N(n�o)\n");
        livro.setDisponivel(sc.next());
       
        
        System.out.print("Digite o autor do Livro: \n");
        livro.setAutor(sc.next());
        
        System.out.print("Digite o nome do Livro: \n");
        livro.setNome(sc.next());
        
        livroDAO.adiciona(livro);
        System.out.print("Livro adicionado com Sucesso! \n");
	}
	
	
	private static void adicionaLocacao(){
		Scanner sc = new Scanner(System.in);
		Locacao locacao = new Locacao(); 
	        
	    System.out.print("Digite o cupom da loca��o: \n");
	    locacao.setCupom(sc.nextLine());
	    
	    System.out.print("Escolha o locador abaixo, digite seu cpf: \n\n");
	    listarUsuarios();
	    locacao.setCpf(sc.nextLine());
	    
	    System.out.print("Escolha um dos Livros abaixo, digite seu codigo: \n\n");
	    listarLivros();
	    locacao.setCodLivro(sc.nextInt());
	    
	    System.out.print("Data para develu��o do Livro: \n");
	    String dataDevolucao = sc.next();
	    
	    locacao.setDataPreviaDevolucao(converteStringEmData(dataDevolucao));
	    locacaoDAO.adiciona(locacao);
	}
	
	private static void listarUsuarios(){
		List<Usuario> usuarios = new ArrayList<Usuario>(0);
		try {
			usuarios = (List<Usuario>) usuarioDAO.getListaUsuario();
		} catch (SQLException e) {
			e.getMessage();
		}
		int count = 0;
		for (Usuario usuario : usuarios) {
			System.out.println(count+" - "+usuario.getNome()+" - "+usuario.getCpf());
			count++;
		}
	}
	
	private static void listarLivros(){
		List<Livro> livros = new ArrayList<Livro>();
		try {
			livros = (List<Livro>) livroDAO.getListaLivro();
		} catch (SQLException e) {
			e.getMessage();
		}
		int count = 0;
		for (Livro livro : livros) {
			System.out.println(count+" - "+livro.getCodLivro()+" - "+livro.getNome());
		}
 	}
	
	private static void listarLocacoes(){
		List<Locacao> locacoes = new ArrayList<Locacao>();
		try {
			locacoes = (List<Locacao>) locacaoDAO.getListaLocacao();
		} catch (SQLException e) {
			e.getMessage();
		}
		int count = 0;
		for (Locacao loc : locacoes) {
			System.out.println("Cupom - "+loc.getCupom()+" CPF locador - "+loc.getCpf());
		}
	}
	
	private static void deletarUsuario(){
		System.out.println("Digite o CPF do usuario que deseja excluir: /n");
		Scanner sc = new Scanner(System.in);
		usuarioDAO.excluir(sc.nextLine());
		System.out.println("Usuario ecluido com sucesso : /n");
	}
	
	private static void deletarLivro(){
		System.out.println("Digite o codigo do livro que deseja excluir: /n");
		Scanner sc = new Scanner(System.in);
		livroDAO.excluir(sc.nextInt());
		System.out.println("Livro excluido com sucesso : /n");
	}
	
	
	
	public static void main(String[] args) {
		
		//VAI DESCOMENTANDO OS METODOS E VENDO COMO FUNCIONA E TESTA, PODE SER QUE DER ALGUM PAU AI VC J� VAI TENTANDO CORRIGIR
		//J� ESTA TUDO PRONTO S� N�O DEU TEMPO DE TESTAR TUDO
		
		//para listar o usuario por CPF s� tirar o comentario desta linha
//		listaUsuarioPorCpf("06455017439");
		
		//aqui chama o metodo para adicionar um novo usuario pegado valores do console com scanner
//		adicionarUsuario();
		//lista usuarios
//		listarUsuarios();
		//deletar usuario
//		deletarUsuario();
		
		//adicionar um livro
		adicionaLivro();
		//lista livros
//		listarLivros();
		//deletar livro
//		deletarLivro();
		
		//lista loca��es
//		listarLocacoes();
		//adiciona loca��es
//		adicionaLocacao();

	}

}
